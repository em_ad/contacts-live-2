package com.example.contactslive2;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    private static App context;
    private static String version;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        version = "-1";
    }

    public static String getVersion() {
        return version;
    }

    public static void setVersion(String v) {
        version = v;
    }

    public static Context getContext() {
        return context;
    }
}
