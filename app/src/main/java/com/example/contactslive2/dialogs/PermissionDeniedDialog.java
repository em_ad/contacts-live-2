package com.example.contactslive2.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.example.contactslive2.R;
import com.example.contactslive2.interfaces.PermissionDialogInterface;

public class PermissionDeniedDialog extends Dialog implements View.OnClickListener {

    public PermissionDialogInterface dialogInterface;

    public PermissionDeniedDialog(@NonNull Context context, PermissionDialogInterface dialogInterface) {
        super(context);
        this.dialogInterface = dialogInterface;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_permission);
        findViewById(R.id.tvExit).setOnClickListener(this);
        findViewById(R.id.tvRetry).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvRetry:
                dialogInterface.retry();
                dismiss();
                break;
            case R.id.tvExit:
                dialogInterface.exit();
                dismiss();
                break;
        }
    }
}
