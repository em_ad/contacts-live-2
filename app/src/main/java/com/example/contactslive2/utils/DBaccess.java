package com.example.contactslive2.utils;

import android.util.Log;

import androidx.room.Room;

import com.example.contactslive2.App;
import com.example.contactslive2.interfaces.DBinterface;
import com.example.contactslive2.model.Contact;

import java.util.Collections;
import java.util.List;

public class DBaccess {

    private DBinterface db;

    private static DBaccess instance = null;

    public DBaccess() {
        db = Room.databaseBuilder(App.getContext(), DBinterface.class, "room_2").allowMainThreadQueries().build();
        instance = this;
    }

    public static DBaccess getInstance() {
        if (instance == null)
            instance = new DBaccess();
        return instance;
    }

    public DBinterface getDb() {
        return db;
    }

    public static void insertContactsIfNeeded(List<Contact> newList) {
        List<Contact> dbList = DBaccess.getInstance().getDb().contactDao().getAllSimple();

        Collections.sort(dbList);
        Collections.sort(newList);

        int j = 0;

        Boolean biggerI = null;

        if (dbList.size() == 0) {
            DBaccess.getInstance().getDb().contactDao().insertAll(newList);
        } else {
            for (int i = 0; i < Math.max(newList.size(), dbList.size()); ) {
                if (j >= dbList.size() - 1) {
                    if (i < newList.size() - 1)
                        i++;
                    else break;
                    DBaccess.getInstance().getDb().contactDao().insert((newList.get(i)));
                    continue;
                }

                if (i >= newList.size() - 1) {
                    if (j < dbList.size() - 1)
                        j++;
                    else break;
                    if (dbList.get(j).compareTo(newList.get(i - 1)) > 0) {
                        DBaccess.getInstance().getDb().contactDao().delete(dbList.get(j));
                    }
                    continue;
                }

                if (newList.get(i).compareTo(dbList.get(j)) > 0) {
                    if (biggerI != null && !biggerI) {
                        DBaccess.getInstance().getDb().contactDao().delete(dbList.get(j));
                    }
                    biggerI = true;
                    if (j < dbList.size() - 1)
                        j++;
                } else if (newList.get(i).compareTo(dbList.get(j)) < 0) {
                    biggerI = false;
                    DBaccess.getInstance().getDb().contactDao().insert((newList.get(i)));
                    if (i < newList.size() - 1)
                        i++;
                    else break;
                } else {
                    biggerI = null;
                    if (!newList.get(i).equals(dbList.get(j))) {
                        DBaccess.getInstance().getDb().contactDao().update(newList.get(i));
                        if (i < newList.size() - 1)
                            i++;
                        else break;
                    } else { // no update needed
                        if (i < newList.size() - 1)
                            i++;
                        else break;
                    }
                }
            }
        }
    }
}
