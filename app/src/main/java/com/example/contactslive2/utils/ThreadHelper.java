package com.example.contactslive2.utils;

import android.os.AsyncTask;

public class ThreadHelper {

    static Thread thread;

    public static synchronized void runOnSingleThread(Runnable r){
        thread = new Thread(r);
        thread.start();
    }

}
