package com.example.contactslive2.utils;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.example.contactslive2.model.Contact;

import java.util.List;

public class DataUtils extends PageKeyedDataSource<Integer, Contact> {

    public static final int PAGE_SIZE = 20;
    private ContactListProvider provider;

    public DataUtils(ContactListProvider provider) {
        this.provider = provider;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Contact> callback) {
        List<Contact> result = provider.getStringList(0, params.requestedLoadSize);
        callback.onResult(result, 1, 2);
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Contact> callback) {
        List<Contact> result = provider.getStringList(params.key, params.requestedLoadSize);
        Integer nextIndex = null;

        if (params.key > 1) {
            nextIndex = params.key - 1;
        }
        callback.onResult(result, nextIndex);
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Contact> callback) {
        List<Contact> result = provider.getStringList(params.key, params.requestedLoadSize);
        callback.onResult(result, params.key + 1);
    }
}
