package com.example.contactslive2.utils;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.contactslive2.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContentHelper {

    private static ContentResolver resolver;

    public static void setContentResolver(ContentResolver cr){
        resolver = cr;
    }

    public synchronized static void getContactList(ContentResolver cr) {
        final List<Contact> simpleList = new ArrayList<>();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    String phoneNo = "";
                    while (pCur.moveToNext()) {
                        phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }
                    pCur.close();
                    simpleList.add(new Contact(id, name, phoneNo));
                }
            }
        }
        if (cur != null) {
            cur.close();
        }
        DBaccess.insertContactsIfNeeded(simpleList);

    }

    public static void listenOnContacts(final ContentResolver cr) {
        cr.registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                super.onChange(selfChange);
                checkModifications();
            }
        });
    }

    public synchronized static void checkModifications() {
        if(resolver == null)
            return;
        ThreadHelper.runOnSingleThread(checkContactsRunnable);
    }

    static Runnable checkContactsRunnable = new Runnable() {
        @Override
        public void run() {
            ContentHelper.getContactList(resolver);
        }
    };

}
