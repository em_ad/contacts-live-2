package com.example.contactslive2.utils;

import com.example.contactslive2.model.Contact;

import java.util.List;

public class ContactListProvider {

    private List<Contact> list;

    public ContactListProvider(List<Contact> list) {
        this.list = list;
    }

    public List<Contact> getStringList(int page, int pageSize) {
        int initialIndex = page * pageSize;
        int finalIndex = initialIndex + pageSize;

        if(finalIndex >= list.size())
            finalIndex = list.size() - 1;

        return list.subList(initialIndex, finalIndex);
    }
}