package com.example.contactslive2.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.contactslive2.R;
import com.example.contactslive2.interfaces.AdapterTestingInterface;
import com.example.contactslive2.model.Contact;

public class PagedContactsAdapter extends PagedListAdapter<Contact, PagedContactsAdapter.ViewHolder> {

    AdapterTestingInterface testingInterface;

    public PagedContactsAdapter() {
        super(new DiffUtil.ItemCallback<Contact>() {
            @Override
            public boolean areItemsTheSame(@NonNull Contact oldItem, @NonNull Contact newItem) {
                return newItem.getId().equals(oldItem.getId());
            }

            @Override
            public boolean areContentsTheSame(@NonNull Contact oldItem, @NonNull Contact newItem) {
                return newItem.equals(oldItem);
            }
        });
    }

    public void setTestingInterface(AdapterTestingInterface testingInterface) {
        this.testingInterface = testingInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(testingInterface != null)
            testingInterface.onCreateViewHolderCalled();
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_contact_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Contact model = getItem(position);
        if(testingInterface != null)
            testingInterface.onBindIsCalled(model);
        if (model != null) {
            holder.tvName.setText(model.getName());
            holder.tvNumber.setText(model.getNumber());
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvNumber;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvNumber = itemView.findViewById(R.id.tvNumber);
        }
    }
}