package com.example.contactslive2.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.DataSource;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.example.contactslive2.utils.DBaccess;
import com.example.contactslive2.model.Contact;

import java.util.List;

public class ContactViewModel extends ViewModel {

    public LiveData<PagedList<Contact>> liveData;


    public ContactViewModel() {
        liveData = new LivePagedListBuilder<>(DBaccess.getInstance().getDb().contactDao().getAll(), 50).build();
    }

    public void clearData(){
        if(liveData.getValue() != null)
            this.liveData.getValue().clear();
    }

    public void setLiveData(LiveData<PagedList<Contact>> liveData){
        this.liveData = liveData;
    }
}
