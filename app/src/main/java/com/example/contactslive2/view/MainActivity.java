package com.example.contactslive2.view;

import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.contactslive2.R;
import com.example.contactslive2.adapters.PagedContactsAdapter;
import com.example.contactslive2.dialogs.PermissionDeniedDialog;
import com.example.contactslive2.interfaces.PermissionDialogInterface;
import com.example.contactslive2.model.Contact;
import com.example.contactslive2.utils.Constants;
import com.example.contactslive2.utils.ContentHelper;
import com.example.contactslive2.utils.PermissionHelper;
import com.example.contactslive2.utils.ThreadHelper;
import com.example.contactslive2.viewmodel.ContactViewModel;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private PagedContactsAdapter adapter;
    private ProgressBar progress;
    private ContactViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        viewModel.liveData.observe(this, new Observer<PagedList<Contact>>() {
            @Override
            public void onChanged(PagedList<Contact> contacts) {
                if (contacts.size() > 0)
                    hideProgress();
                else showProgress();
                adapter.submitList(contacts);
            }
        });
        findViews();
        setupRecycler();
        if (!PermissionHelper.checkContactsPermission(this))
            PermissionHelper.requestContactsPermission(this);
        else {
            checkThreadAndGetContacts();
        }
    }

    private void checkThreadAndGetContacts() {
        ThreadHelper.runOnSingleThread(new Runnable() {
            @Override
            public void run() {
                ContentHelper.getContactList(getContentResolver());
                Looper.prepare();
                ContentHelper.setContentResolver(getContentResolver());
                ContentHelper.listenOnContacts(getContentResolver());
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Constants.CONTACT_PERMISSION_CODE: {
                if (PermissionHelper.permissionGranted(grantResults)) {
                    checkThreadAndGetContacts();
                } else {
                    showProgress();
                    showDialog();
                }
                break;
            }
        }
    }

    public PermissionDeniedDialog showDialog() {
        PermissionDeniedDialog dialog = new PermissionDeniedDialog(this, new PermissionDialogInterface() {
            @Override
            public void retry() {
                PermissionHelper.requestContactsPermission(MainActivity.this);
            }

            @Override
            public void exit() {
                finishAffinity();
            }
        });
        dialog.show();
        return dialog;
    }


    public void setupRecycler() {
        adapter = new PagedContactsAdapter();
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(this));
    }

    private void findViews() {
        recycler = findViewById(R.id.recycler);
        progress = findViewById(R.id.progress);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PermissionHelper.checkContactsPermission(this)) {
            ContentHelper.setContentResolver(getContentResolver());
            ContentHelper.listenOnContacts(getContentResolver());
        }
    }

    private void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progress.setVisibility(View.GONE);
    }
}
