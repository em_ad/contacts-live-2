package com.example.contactslive2.interfaces;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.contactslive2.model.Contact;

@Database(entities = {Contact.class}, version = 1, exportSchema = false)
public abstract class DBinterface extends RoomDatabase {
    public abstract ContactsDao contactDao();
}
