package com.example.contactslive2.interfaces;

import com.example.contactslive2.model.Contact;

public interface AdapterTestingInterface {
    void onBindIsCalled(Contact contact);
    void onCreateViewHolderCalled();
}
