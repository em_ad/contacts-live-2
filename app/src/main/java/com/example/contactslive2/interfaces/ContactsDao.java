package com.example.contactslive2.interfaces;

import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.contactslive2.model.Contact;

import java.util.List;

@Dao
public interface ContactsDao {

    @Query("SELECT * FROM contacts_db")
    DataSource.Factory<Integer, Contact> getAll();

    @Query("SELECT * FROM contacts_db")
    List<Contact> getAllSimple();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Contact> contacts);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Contact contacts);

    @Update
    void updateAll(List<Contact> contacts);

    @Update
    void update(Contact contact);

    @Delete
    void delete(Contact user);

}