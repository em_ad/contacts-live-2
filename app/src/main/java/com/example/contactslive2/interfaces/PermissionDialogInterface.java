package com.example.contactslive2.interfaces;

public interface PermissionDialogInterface {

    void retry();

    void exit();

}
