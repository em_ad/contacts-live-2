package com.example.contactslive2.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "contacts_db")
public class Contact implements Serializable, Comparable {

    @PrimaryKey
    @NonNull
    private String id;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "number")
    private String number;


    @NonNull
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public Contact(String id, String name, String number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }

    @Override
    public int compareTo(Object o) {
        Contact co = (Contact) o;
        if (Integer.valueOf(id) < Integer.valueOf(co.getId()))
            return -1;
        if (Integer.valueOf(id) == Integer.valueOf(co.getId()))
            return 0;
        if (Integer.valueOf(id) > Integer.valueOf(co.getId()))
            return 1;
        return 0;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        Contact co = (Contact) obj;
        if (co.getId().equals(id) && co.getName().equals(name) && co.getNumber().equals(number))
            return true;
        else return false;
    }
}

