package com.example.contactslive2.utils;

import android.os.Looper;

import com.example.contactslive2.view.MainActivity;

import org.junit.Rule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.RobolectricTestRunner;

public class ThreadHelperTestWrapper extends ThreadHelper {

    public Thread exposeThread(){
        return thread;
    }

    public Looper exposeLooper(){
        return Looper.myLooper();
    }


    public void runOnThread(Runnable runnable){
        runOnSingleThread(runnable);
    }

}
