package com.example.contactslive2.utils;

import android.os.Handler;
import android.os.Looper;

import com.example.contactslive2.view.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;

import static org.junit.Assert.*;

@Config(manifest= Config.NONE)
@RunWith(RobolectricTestRunner.class)
public class ThreadHelperTest {

    Thread mThread = null;

    private static final String TAG = "TestProcess";

    @Mock
    MainActivity activity = null;

    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(MainActivity.class).get();
        threadHelper = new ThreadHelperTestWrapper();
        mThread = threadHelper.exposeThread();
    }

    @After
    public void tearDown() throws Exception {
        mThread = null;
    }

    @Mock
    ThreadHelperTestWrapper threadHelper = null;

    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            //check looper differentiation
            assertFalse(threadHelper.exposeLooper() == activity.getMainLooper());
        }
    };

    @Test
    public void checkLooper(){
        //static context has main looper
        assertTrue(threadHelper.exposeLooper() == activity.getMainLooper());
        //instantiating thread changes the looper
        mThread = new Thread(runnable); mThread.start();
    }

    ArrayList<Integer> lockRes = new ArrayList<>();

    @Test
    public void checkThreadLock(){
        //runnable checks for lock integrity
        lockRes.clear();
        threadHelper.runOnThread(new Runnable() {
            @Override
            public void run() {
                ShowAndCheck(1);
                Looper.prepare();
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        ShowAndCheck(3);
                    }
                });
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        ShowAndCheck(4);
                    }
                });
                //checking thread to be locked
                threadHelper.runOnThread(new Runnable() {
                    @Override
                    public void run() {
                        ShowAndCheck(6);
                        threadHelper.runOnThread(new Runnable() {
                            @Override
                            public void run() {
                                ShowAndCheck(8);

                            }
                        });
                        ShowAndCheck(7);

                    }
                });
                ShowAndCheck(5);

            }
        });
        //check thread to be unlocked again
        threadHelper.runOnThread(new Runnable() {
            @Override
            public void run() {
                ShowAndCheck(2);

            }
        });
    }

    private synchronized void ShowAndCheck(int num){
        lockRes.add(num);
        if(lockRes.size() == 7){
            for (int i = 0; i < lockRes.size(); i++) {
                if(i != 0) //checking runnables to have been run in the right sequence
                    assertTrue(lockRes.get(i) > lockRes.get(i-1));
            }
        }
    }

}