package com.example.contactslive2.adapters;


import android.os.Handler;
import android.os.SystemClock;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.DataSource;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.platform.app.InstrumentationRegistry;

import com.example.contactslive2.R;
import com.example.contactslive2.interfaces.AdapterTestingInterface;
import com.example.contactslive2.model.Contact;
import com.example.contactslive2.utils.DBaccess;
import com.example.contactslive2.utils.ThreadHelper;
import com.example.contactslive2.view.MainActivity;
import com.example.contactslive2.viewmodel.ContactViewModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLooper;

import java.util.ArrayList;
import java.util.concurrent.Executor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.robolectric.shadows.ShadowInstrumentation.getInstrumentation;

@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner.class)
public class adapterTest {

    final static int DB_TEST_SIZE = 5;

    @Mock
    MainActivity activity = null;

    @Mock
    PagedContactsAdapter adapter = null;

    @Mock
    ContactViewModel viewModel;

    @Mock
    PagedList<Contact> pagedStrings;

    ArrayList<Object> arrayList = new ArrayList<>();

    int holders = 0;

    @Before
    public void prepareAdapter() {
        activity = Robolectric.buildActivity(MainActivity.class).get();
        viewModel = ViewModelProviders.of(activity).get(ContactViewModel.class);
        adapter = new PagedContactsAdapter();
        for (int i = 0; i < DB_TEST_SIZE; i++) {
            DBaccess.getInstance().getDb().contactDao().insert(new Contact(String.valueOf((int) (Math.random() * 1000)), "test", "test"));
        }
    }

    @After
    public void releaseRes() {
        activity = null;
        viewModel = null;
        adapter = null;
    }

    @Test
    public void testAdapter() throws InterruptedException { //KASIF CODING FTW :)
        assertTrue(adapter.getItemCount() == 0);
        //view model is empty when being created
        assertFalse(viewModel.liveData.getValue() != null && viewModel.liveData.getValue().size() > 0);
        final PagedList.Config myConfig = new PagedList.Config.Builder()
                .setInitialLoadSizeHint(50)
                .setPageSize(50)
                .build();

        DataSource<Integer, Contact> integerContactDataSource = DBaccess.getInstance().getDb().contactDao().getAll().create();
        pagedStrings = new PagedList.Builder<>(integerContactDataSource, myConfig)
                .setInitialKey(0).setNotifyExecutor(new Executor() {
                    @Override
                    public void execute(Runnable runnable) {
                        new Thread(runnable).start();
                    }
                }).setFetchExecutor(new Executor() {
                    @Override
                    public void execute(Runnable runnable) {
                        new Thread(runnable).start();
                    }
                })
                .build();
        adapter.submitList(pagedStrings);
        assertTrue(adapter.getItemCount() == DBaccess.getInstance().getDb().contactDao().getAllSimple().size());
        assertTrue(adapter.getItemCount() == DB_TEST_SIZE);

    }

}
