package com.example.contactslive2.view;

import android.Manifest;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;

import com.example.contactslive2.R;
import com.example.contactslive2.adapters.PagedContactsAdapter;
import com.example.contactslive2.dialogs.PermissionDeniedDialog;
import com.example.contactslive2.interfaces.AdapterTestingInterface;
import com.example.contactslive2.model.Contact;
import com.example.contactslive2.utils.DBaccess;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import kotlin.jvm.JvmField;

import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MainActivityTest {

    private static final String TAG = "TestProcess";

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    MainActivity activity = null;

    LiveData<PagedList<Contact>> liveData;

    @Before
    public void setUp() throws Exception {
        activity = activityTestRule.getActivity();
    }

    @After
    public void tearDown() throws Exception {
        activity.finish();
        activity = null;
    }

    @Test
    public void checkActivityBuild() {
        //check activity creation
        final int initialDBSize = DBaccess.getInstance().getDb().contactDao().getAllSimple().size();
        assertNotNull(activity.findViewById(R.id.progress));
        assertNotNull(activity.findViewById(R.id.recycler));
        final RecyclerView rec = (RecyclerView) activity.findViewById(R.id.recycler);
        liveData = new LivePagedListBuilder(DBaccess.getInstance().getDb().contactDao().getAll(), 50).build();
        assertTrue(liveData.getValue() == null || liveData.getValue().size() == 0);
        new Handler(activity.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                liveData.observe(activity, new Observer<PagedList<Contact>>() {
                    @Override
                    public void onChanged(PagedList<Contact> contacts) {
                        assertTrue(initialDBSize == contacts.size() - 1);
                        assertFalse(liveData.getValue() == null || liveData.getValue().size() == 0);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                SystemClock.sleep(500); //db delay
                                assertTrue(rec.getAdapter().getItemCount() == DBaccess.getInstance().getDb().contactDao().getAllSimple().size());
                            }
                        }).start();
                    }
                });
            }
        });
        ((PagedContactsAdapter) rec.getAdapter()).setTestingInterface(new AdapterTestingInterface() {
            @Override
            public void onBindIsCalled(Contact contact) {
                System.out.println("view bound = " + contact.getName());
            }

            @Override
            public void onCreateViewHolderCalled() {
                System.out.println("adapter size = " + rec.getAdapter().getItemCount());
            }
        });
        if (Looper.myLooper() == null)
            Looper.prepare();
        new Thread(new Runnable() {
            @Override
            public void run() {
                DBaccess.getInstance().getDb().contactDao().insert(new Contact(String.valueOf((int) (Math.random() * 1000)), "test", "test"));
            }
        }).start();
    }

    @Test
    public void checkDialog() {
        if (Looper.myLooper() == null)
            Looper.prepare();
        PermissionDeniedDialog exposedDialog = null;
        exposedDialog = activity.showDialog();
        assertTrue(exposedDialog != null);
        exposedDialog.dialogInterface.retry();
        assertTrue(activity != null);
        exposedDialog.dismiss();
        Espresso.onView(withId(R.layout.dialog_permission)).check(doesNotExist());
    }
}