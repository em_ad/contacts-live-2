package com.example.contactslive2.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import androidx.core.content.ContextCompat;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiSelector;

import com.example.contactslive2.PermissionTestActivity;
import com.example.contactslive2.view.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.ArrayList;

import kotlin.jvm.JvmField;

import static androidx.test.InstrumentationRegistry.getTargetContext;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.*;

public class PermissionHelperTest {

//    @Rule
//    @JvmField
//    public GrantPermissionRule grantPermissionRule  = GrantPermissionRule.grant(Manifest.permission.READ_CONTACTS);

    @Rule
    public ActivityTestRule<PermissionTestActivity> activityTestRule = new ActivityTestRule<>(PermissionTestActivity.class);

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void checkPermission() {
        if (PermissionHelper.checkContactsPermission(getInstrumentation().getTargetContext()))
            assertTrue(ContextCompat.checkSelfPermission(getInstrumentation().getTargetContext(), Manifest.permission.READ_CONTACTS)
                    == PackageManager.PERMISSION_GRANTED);
        else
            assertFalse(ContextCompat.checkSelfPermission(getInstrumentation().getTargetContext(), Manifest.permission.READ_CONTACTS)
                    == PackageManager.PERMISSION_GRANTED);
    }

    @Test
    public void checkGrant() {

//        getInstrumentation().getUiAutomation().executeShellCommand("pm reset-permissions com.example.contactslive2");
//        SystemClock.sleep(500);


        final Activity activity = activityTestRule.getActivity();
        assertTrue("Please Remove Contact Permission From App!", activity.checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED);

        ArrayList<String> permissions = new ArrayList<>();
        permissions.add(Manifest.permission.READ_CONTACTS);
        for (int i = 0; i < permissions.size(); i++) {
            String command = String.format("pm grant %s %s", getInstrumentation().getTargetContext().getPackageName(), permissions.get(i));
            getInstrumentation().getUiAutomation().executeShellCommand(command);
            // wait a bit until the command is finished
            SystemClock.sleep(500);
        }

        assertTrue(activity.checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED);
    }

    @Test
    public void myGranter() throws InterruptedException {
        Looper.prepare();

        PermissionHelper.requestContactsPermission(activityTestRule.getActivity());

        if (Build.VERSION.SDK_INT >= 23) {
            UiDevice mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
            UiObject allowPermissions = mDevice.findObject(new UiSelector().text("ALLOW"));
            if (allowPermissions.exists()) {
                try {
                    allowPermissions.click();
                    System.out.println("CLICKED ");

                } catch (UiObjectNotFoundException e) {
                    System.out.println("There is no permissions dialog to interact with ");
                }
            } else {
                System.out.println("ELEMENT NON EXISTENT");
            }
        }
        SystemClock.sleep(500);
        assertTrue(PermissionHelper.checkContactsPermission(activityTestRule.getActivity()));

    }


}