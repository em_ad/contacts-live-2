package com.example.contactslive2.utils;

import android.util.Log;

import com.example.contactslive2.interfaces.DBinterface;
import com.example.contactslive2.model.Contact;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class DBaccessTest {

    List<Contact> dbContacts = null;

    private static final String TAG = "TestProcess";

    @Before
    public void setUp() throws Exception {
        //make contacts list
        try {
            dbContacts = DBaccess.getInstance().getDb().contactDao().getAllSimple();
        } catch (Exception e) {
            throw e;
        }
    }

    @After
    public void tearDown() throws Exception {
        //destroy the reference
        dbContacts = null;
    }

    @Test
    public void checkSize() {
        //Check if Size if reliable
        assertTrue(DBaccess.getInstance().getDb().contactDao().getAllSimple().size() == dbContacts.size());
        //Show DB size to tester
        Log.e(TAG, "DB SIZE: " + dbContacts.size());
        for (int i = 0; i < dbContacts.size(); i++) {
            //check if IDs are not null
            assertNotNull(dbContacts.get(i).getId());
        }
    }

    @Test
    public void operationTest() {
        //check contact constructor
        Contact dummy = new Contact("test", "test", "test");
        //check entry insertion
        DBaccess.getInstance().getDb().contactDao().insert(dummy);
        assertTrue(DBaccess.getInstance().getDb().contactDao().getAllSimple().size() - 1 == dbContacts.size());
        //check object integrity
        Log.e(TAG, "operationTest: " + DBaccess.getInstance().getDb().contactDao().getAllSimple().get(dbContacts.size()).getId());
        //check entry removal
        DBaccess.getInstance().getDb().contactDao().delete(dummy);
        //check operation finish
        assertTrue(DBaccess.getInstance().getDb().contactDao().getAllSimple().size() == dbContacts.size());
        assertTrue(!DBaccess.getInstance().getDb().contactDao().getAllSimple().contains(dummy));
    }
}